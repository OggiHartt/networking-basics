# List of resources 

First source Cloudflare i see as quite an authoritative source as they are a business that runs on internet security etc and in their guides they have no plugs to their business or services and the guides are in depth with good descriptions and models to demonstrate the topic in an easy to digest way. 

https://www.cloudflare.com/learning/ddos/glossary/open-systems-interconnection-model-osi/

This link gives a very broad and easy to understand definition of the seven layers of the OSI model; it isn't very in-depth but helps give a better understanding to the entire process of data flow from 1 computer to another and gives each of the seven layers a quick and easy to understand explanation.


https://www.youtube.com/watch?v=Ilk7UXzV_Qc

Another simple yet insightful source that gives an easy overview of the different layers, and describes some of the different abbreviations that are being talked about very slightly such as TCP and UDP. gives good examples and has a nice flow and good visuals to help with understanding the OSI layers 

https://www.geeksforgeeks.org/layers-of-osi-model/

A very nice in depth description of the OSI model with great definition of terms and examples of which devices are used for each of the seven layers. In this article it also highlights important terms making it easy to also google those to get an even better understanding of the entire process and help gain even more knowledge on the subject. 


https://www.youtube.com/watch?v=cA9ZJdqzOoU

I felt like these 2 terms TCP and UDP were being used and i personally had no previous knowledge about what they meant so i found this video which give a very nice in-depth explanation on the differences between the 2 and what they are used for. 







