# There are 3 types of network sniffing tools or methods as far as i understand.

The 1st would be setting up a process on either the client, server or a router somewhere along the data stream and then capturing the packages and then we can decide if we want to save them as a PCAP or use an online interface etc.

The 2nd would be creating a mirror port at the switch which will then also send the data stream to the collector (NSM) that we can then use to see the data. If it is a large data flow 2x 1gbit/s then you need to set up multiple mirror ports to actually receive all the data. This would be classified as the 2nd layer or Data Link Layer (DLL)  and this is because we would be creating a port on a switch which is mainly used for data linking. 

The 3rd would be setting up a hardware tap which could then also send the data stream to a collector (NSM). this is being done on the 1st layer because we are tapping into a physical connection with a different device




